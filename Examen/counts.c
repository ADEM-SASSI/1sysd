#include <stdio.h>


int count_char(char *t1, char c) {
    int nb = 0; 
    for (int i = 0; t1[i] != '\0'; i++) { 
        if (t1[i] == c) { 
            nb++; 
        }
    }
    return nb; 
}
int count_words(char *t3) {
    int nb3 = 0;  
    int in_world = 0;  

    for (int i = 0; t3[i] != '\0'; i++) {      
        if (in_world == 0) {
                nb3++;
                in_world = 1;
            }
         else {
            in_world = 0;
        }
    }


    return nb3;
}
int count_words_better(char *t3) {
    int nb3 = 0;  
    int in_world = 0;  
    for (int i = 0; t3[i] != '\0'; i++) {
        if (t3[i] != ' ' && t3[i] != '\t' && t3[i] != '\n') {
            
             if (in_world == 0) {
               
                nb3++;
                in_world = 1; 
                }
                } 
         else {
            
           
            in_world = 0;  
        }
    }

    return nb3;
}


int main() {
    char t2[50];
    char carac; 
    printf("Entrez une chaine de caracteres  : ");
    fgets(t2, sizeof(t2), stdin); 
    printf("Entrez le caractere rechercher : ");
    scanf("%c", &carac); 

    
    int nb2 = count_char(t2, carac); 
    printf("Le caractere '%c' apparait %d fois dans la chaine\n", carac, nb2);
    int nb4 = count_words(t2);
    printf("Nombre de mots dans la chaine %s: %d\n", t2, nb4);
    int nb5 = count_words_better(t2);
    printf("Nombre de mots dans la chaine %s: %d\n", t2, nb5);


    return 0;
}

