#include <stdio.h>
int i;
void small_to_zero(int *t, int n, int val) {
	int i ;
    for ( i = 0; i < n; i++) {
        if (t[i] <= val) {
            t[i] = 0;
        }
    }
}

int main() {
    int t[] = {1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0};
    int n = sizeof(t) / sizeof(t[0]); 

    printf("\tTableau avant l'appel de la fonction :   ");
    for ( i = 0; i < n; i++) {
        printf("%d  ", t[i]);
    }

    int val = 3; 
    small_to_zero(t, n, val); 

    printf("\n\tTableau apres l'appel de la fonction :   ");
    for (i = 0; i < n; i++) {
        printf("%d  ", t[i]);
     
    }
	printf("\n");   
    return 0;
}

