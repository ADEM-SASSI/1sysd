#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node *next;
    struct node *prev;
} node;

node *create_node(int val) {
    node *newnode = malloc(sizeof(node));
    if (!newnode) {
        fprintf(stderr, "Error: malloc failed in create_node\n");
        exit(1);
    }
    newnode->val = val;
    newnode->next = NULL;
    newnode->prev = NULL;
    return newnode;
}
void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

void append_val(node **phead, int val) {
    node *new_node = create_node(val);
    if (!(*phead)) { 
        *phead = new_node;
    } else {
        node *walk = *phead;
        while (walk->next) {
            walk = walk->next;
        }
        walk->next = new_node;
        new_node->prev = walk;
    }
}

void forth_and_back(node *head) {
    node *previous  = NULL;
    node *walk = head;
    printf("Forward: ");
    while (walk) {
        printf("%d ", walk->val);
        previous  = walk;
        walk = walk->next;
    }
    printf("\nBackward: ");
    while (previous ) {
        printf("%d ", previous ->val);
        previous  = previous ->prev;
    }
    printf("\n");
}

int main() {
    node *head = NULL;
    append_val(&head, 42);
    append_val(&head, 12);
    append_val(&head, 54);
    append_val(&head, 41);
    print_list(head);
    forth_and_back(head);
    return 0;
}

