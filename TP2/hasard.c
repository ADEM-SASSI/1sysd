#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(NULL));
    int hasard = rand() % 10 + 1;
    int uti;

    printf("entrez un nb de 1 a 10 ");
    scanf("%d", &uti);

    if (uti < hasard) {
        printf("plus petit\n");
    } else if (uti > hasard) {
        printf("plus grand\n");
    } else {
        printf("trouvé\n");
    }
     printf("le nb est: %d\n", hasard);
    return 0;
}

