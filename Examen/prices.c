#include <stdio.h>

int main() {
    float prix1, prix2,pourcentage;
  

    
    printf("Entrez le premier prix : ");
    scanf("%f", &prix1);
    printf("Entrez le deuxieme prix : ");
    scanf("%f", &prix2);

   
    pourcentage = ((prix2 - prix1) / prix1) * 100;

   
    if (pourcentage > 0) {
        printf("Le pourcentage d'augmentation  est : %.2f%%\n", pourcentage);
    } else if (pourcentage < 0) {
        printf("Le pourcentage de baisse  est : %.2f%%\n", -pourcentage);
    } else {
        printf("Les deux prix sont identiques.\n");
    }

    return 0;
}

