#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void add_vectors(int n, double *T1, double *T2, long double *T3) {
    for (int i = 0; i < n; i++) {
        long double differences  = T1[i] - T2[i]; 
        T3[i] = pow(differences, 2); 
    }
}

double norm_vector(int n, long double *t) { 
    long double somme = 0.0;
    for (int i = 0; i < n; i++) {
        somme += (t[i] * t[i]); 
    }
    double norm = sqrt(somme); 
    return norm;
}

int main() {
    double T1[] = {3.14, -1.0, 2.3, 0, 7.1};
    double T2[] = {2.71, 2.5, -1, 3, -7};
    long double T3[5];

    int n = 5;

    printf("T1 : ");
    for (int i = 0; i < n; i++) {
        printf("%+.2lf ", T1[i]);
    }
    printf("\nT2 : ");
    for (int i = 0; i < n; i++) {
        printf("%+.2lf ", T2[i]);
    }
    printf("\n");

    add_vectors(n, T1, T2, T3);

    printf("T3  : ");
    for (int i = 0; i < n; i++) {
        printf("%.2Lf ", T3[i]);
    }
    printf("\n");

    double norm_T3 = norm_vector(n, T3); 
    printf("Norme euclidienne de T3 : %.2lf\n", norm_T3); 

    exit(EXIT_SUCCESS);
}

